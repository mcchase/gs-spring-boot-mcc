/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 22, 2019
 */
package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class TimeController {

  @RequestMapping("/get_time")
  public String index() {
    long millis=System.currentTimeMillis();
    java.util.Date date=new java.util.Date(millis);
    return date.toString();
  }

}

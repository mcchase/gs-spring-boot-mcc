/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 22, 2019
 */
package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

  @RequestMapping("/hello")
  public String index() {
    return "HI!";
  }

}
